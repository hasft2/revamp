/*! [PROJECT_NAME] | Suitmedia */

((window, document, undefined) => {

    const isActive = 'is-active'
    
    const path = {
        img: `${myPrefix}assets/img/`,
        css: `${myPrefix}assets/css/`,
        js : `${myPrefix}assets/js/vendor/`
    };

    const assets = {
        _jquery_cdn     : `https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js`,
        _jquery_local   : `${path.js}jquery.min.js`,
        _fastclick      : `${path.js}fastclick.min.js`,
        _slider         : `${path.js}slick.min.js`,
        _onScreen       : `${path.js}visible.min.js`,
        _parallax       : `${path.js}parallax.min.js`,
        _formValidation : `${path.js}baze.validate.min.js`,
        _selectCustom   : ['https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'],
        _videojs        : ['https://cdn.plyr.io/1.6.20/plyr.js'],
        _datePicker     : ['https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js', 'https://cdnjs.cloudflare.com/ajax/libs/pikaday/1.4.0/css/pikaday.min.css', 'https://cdnjs.cloudflare.com/ajax/libs/pikaday/1.4.0/pikaday.min.js']
    };

    const Site = {

        init() {
            Site.fastClick();
            Site.enableActiveStateMobile();
            Site.WPViewportFix();
            Site.slider()
            Site.tab()
            Site.inputFile()
            Site.accordion()
            Site.tooltip()
            Site.customNavArrows()
            Site.customDots()
            Site.onscreen()
            Site.parallax()
            Site.hamburger()
            Site.smoothScroll()
            Site.formValidation()
            Site.selectCustom()
            Site.videoPlayer()
            Site.headerCompact()
            Site.dropdown()
            Site.popup()
            Site.datePicker()

            window.Site = Site;
        },

        fastClick() {
            Modernizr.load({
                load    : assets._fastclick,
                complete: () => {
                    FastClick.attach(document.body);
                }
            });
        },

        enableActiveStateMobile() {
            if ( document.addEventListener ) {
                document.addEventListener('touchstart', () => {}, true);
            }
        },

        WPViewportFix() {
            if ( navigator.userAgent.match(/IEMobile\/10\.0/) ) {
                let style   = document.createElement('style'),
                    fix     = document.createTextNode('@-ms-viewport{width:auto!important}');

                style.appendChild(fix);
                document.getElementsByTagName('head')[0].appendChild(style);
            }
        },

        slider() {
            let $slider = $('.slider')

            if(!$slider.length) return

            Modernizr.load({
                load: ['https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js'],
                complete: init
            })

            function init () {
                $.each($slider, (i,val) => {
                    if($(val).is('#slider-running-ads')) {
                        $(val).slick({
                            slidesToShow: 4,
                            accessibility: false,
                            responsive: [
                                {
                                    breakpoint: 468,
                                    settings: {
                                        slidesToShow: 1
                                    }
                                }
                            ]
                        })
                    } else {
                        $(val).slick({
                            accessibility: false
                        })
                    }
                })
            }
        },

        tab() {
            let $tab = $('.tab')

            if ( !$tab.length ) return;

            $tab.on('click', '.tab-nav', e => {
                e.preventDefault()

                let $this = $(e.currentTarget)

                if ( $this.hasClass('active') ) return

                let $target = $($this.attr('href'))
                let $tabs = $this.parents('.tab').find('.tab-content')

                $this.siblings().removeClass('active')
                $this.addClass('active')
                $tabs.hide().removeClass(isActive)
                $target.fadeIn(() => {
                    $target.addClass(isActive)
                })

            })
        },

        inputFile() {
            var $inputFile = $('input[type="file"]');

            if(!$inputFile.length) return;

            $inputFile.change(function() {
                var filename = $(this).val();
                var target = $(this).data('inputfile-target');

                $('[data-input-placeholder="' + target +'"]').attr('placeholder', filename);
                $('[data-input-placeholder="' + target +'"]').addClass('input-file-changed');
            })
        },

        accordion() {
            let $accordion = $('.accordion-group')

            if ( !$accordion.length ) return;

            const SLIDE_SPEED = 200;

            $accordion.on('click', '.accordion__title', e => {
                let $this = $(e.currentTarget)
                let $content = $this.next()
                let $parent = $this.parents('.accordion-group')

                if ($parent.data('toggle') === 'single') {
                    if ($this.hasClass(isActive)) return;

                    $parent.find('.accordion__content').slideUp(SLIDE_SPEED);
                    $parent.find('.accordion__title').removeClass(isActive);
                }

                $this.toggleClass(isActive)
                $content.slideToggle(SLIDE_SPEED)
            })
        },

        tooltip() {
            const $tooltip = $('.tooltip')

            if (!$tooltip.length) return

            let $tooltipTrigger = $('.tooltip-container > .btn')

            /*$(document).on('click', function(e) {
                var container = $tooltip

                if(!container.is(e.target) && container.has(e.target).length === 0) {
                    $tooltipTrigger.removeClass(isActive)
                }
            })*/

            $tooltipTrigger.on('click', e => {
                let $this = $(e.currentTarget)

                e.preventDefault()
                e.stopPropagation()
                
                if($this.hasClass(isActive)) {
                    $this.removeClass(isActive)
                } else {
                    $this.parents('.custom-dots').find('.btn').removeClass(isActive)
                    $this.addClass(isActive) 
                }
            })
        },

        customNavArrows() {
            const $customArrows = $('.custom-arrows')

            if(!$customArrows.length) return

            $customArrows.on('click', e => {
                let $this = $(e.currentTarget)
                let $currentSlider = $('#' + $this.data('target-slider'))

                if($this.is('.custom-arrows-next')) $currentSlider.slick('slickNext')
                if($this.is('.custom-arrows-prev')) $currentSlider.slick('slickPrev') 

                
            })
        },

        customDots() {
            const $customDots = $('.custom-dots')

            if(!$customDots.length) return

            let $dots = $customDots.find('.btn')
            let $sliders = $('.custom-dots__container > .slider')

            $dots.on('click', (e) => {
                let $this = $(e.currentTarget)

                let $target = $($this.data('target'))
                $this.parents('.parallax-container').find('.slide__img-bg').hide()
                $target.fadeIn()
            })
        },

        onscreen() {
            const $onScreenTrigger = $('.on-screen')

            if(!$onScreenTrigger.length) return

            Modernizr.load({
                load: assets._onScreen,
                complete: init
            })

            function init() {

                var $animation_elements = $('.elem-animate');
                var $window = $(window);

                function check_if_in_view() {
                  var window_height = $window.height();
                  var window_top_position = $window.scrollTop();
                  var window_bottom_position = (window_top_position + window_height);
                 
                  $.each($animation_elements, function() {
                    var $element = $(this);
                    var element_height = $element.outerHeight();
                    var element_top_position = $element.offset().top;
                    var element_bottom_position = (element_top_position + element_height);
                 
                    //check to see if this current container is within viewport
                    if ((element_bottom_position >= window_top_position) &&
                        (element_top_position <= window_bottom_position)) {
                      $element.addClass('in-view');
                    } else {
                      $element.removeClass('in-view');
                    }
                  });
                }

                $window.on('scroll resize', check_if_in_view);
                $window.trigger('scroll');
            }
        },

        parallax() {
            let $parallax = $('.parallax')

            if(!$parallax.length) return

            Modernizr.load({
                load: assets._parallax,
                complete: init
            })

            function init() {
                $('.parallax-window').parallax();
            }
        },

        hamburger() {
            let $hamburger = $('.menu-trigger')

            if(!$hamburger.length) return

            $hamburger.on('click', e => {
                let $this = $(e.currentTarget)

                $('.utils').toggleClass(isActive);
            })
        },

        smoothScroll() {
            $(function() {
                $('a[href*="#"]:not([href="#"])').click(function() {
                    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                        if (target.length) {
                            $('html, body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                });
            });
        },

        formValidation() {
            let $validForm = $('.form-validation')

            if(!$validForm.length) return

            Modernizr.load({
                load: assets._formValidation,
                complete: init
            })

            function init() {
                $validForm.bazeValidate()
            }
        },

        selectCustom() {
            let $select = $('.select-custom')

            if (!$select.length) return

            Modernizr.load({
                load: assets._selectCustom,
                complete: init
            })

            function init() {
                $select.select2({
                    minimumResultsForSearch: Infinity
                })
            }

        },

        videoPlayer() {
            let $videoPlayer = $('.js-media-player')

            if(!$videoPlayer.length) return

            Modernizr.load({
                load: assets._videojs,
                complete: init
            })

            function init() {
                var player = plyr.setup(document.querySelectorAll('.js-media-player'), {
                    "clickToPlay": false,
                    controls: ['play', 'fullscreen']
                });
                
                player[0].setVolume(10);

                document.querySelector(".js-media-player").addEventListener("playing", function () {
                    /* Magic happens */
                    document.querySelector('button[data-plyr="pause"]').style.pointerEvents = 'none';
                    countVideoWatch()
                });

                document.querySelector(".js-media-player").addEventListener("ended", function () {
                    /* Magic happens */
                    document.querySelector('.plyr__controls').style.pointerEvents = 'auto';
                    $('.question-step').fadeIn()
                });
            }
        },

        headerCompact() {
            let $headerCompact = $('.header-compact')

            if(!$headerCompact.length) return

            window.addEventListener('scroll', function(e){
                var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                    shrinkOn = 300;
                
                if (distanceY > shrinkOn) {
                    $headerCompact.addClass('smaller')
                } else {
                    if ($headerCompact.hasClass('smaller')) {
                        $headerCompact.removeClass('smaller')
                    }
                }
            })
        },

        dropdown() {
            let $dropdown = $('.drop-down')
            let $anchor = $('.drop-down__anchor')

            if(!$dropdown.length) return
            
            $(document).on('click', function(e) {
                var container = $('.drop-down__pop')

                if(!container.is(e.target) && container.has(e.target).length === 0) {
                    $anchor.removeClass('active')
                }
            })

            $anchor.on('click', e => {
                e.preventDefault()
                e.stopPropagation()
                let $this = $(e.currentTarget)

                if($this.hasClass('active')) {
                    $this.removeClass('active')
                } else {
                    $anchor.removeClass('active')
                    $this.addClass('active') 
                }

            })
        },

        popup() {
            var $popBtn = $('.pop-trigger');

            if(!$popBtn.length) return;

            $popBtn.on('click', function() {
                var target = $(this).data('target');
                var domTarget = $(target);

                domTarget.addClass('active');
                $('.popup-overlay').addClass('active');
            })

            $('.btn-close').on('click', function(e) {
                e.preventDefault();
                $('.popup').removeClass('active');
                $('.popup-overlay').removeClass('active');
            })

            $('.popup-overlay').on('click', function() {
                $('.popup').removeClass('active');
                $('.popup-overlay').removeClass('active');  
            })
        },

        datePicker() {
            var $datePicker = $('.date-picker')

            if(!$datePicker.length) return

            Modernizr.load({
                load: assets._datePicker,
                complete: init
            })

            function init() {
                console.log('datePicker')
                var picker = new Pikaday({
                    field: $('#datepicker')[0],
                    format: 'D MMM YYYY',
                    onSelect: function() {
                        console.log(this.getMoment().format('Do MMMM YYYY'));
                    }
                });
            }
        }
    };

    let checkJquery = () => {
        Modernizr.load([
            {
                test    : window.jQuery,
                nope    : assets._jquery_local,
                complete: Site.init
            }
        ]);
    };

    Modernizr.load({
        load    : assets._jquery_local,
        complete: checkJquery
    });

})(window, document);
