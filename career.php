<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div id="site-top" class="parallax sr-only"></div>
<section class="section section-page-banner section--clear">
	<div class="parallax-window" data-parallax="scroll" data-image-src="assets/img/slide-1.jpg"></div>
</section>
<section class="section section-page-content">
	<div class="container">
		<h3 class="h3 section-page__title text-jumbo">Careers</h3>
		<div class="bzg">
			<div class="bzg_c" data-col="m4">
				<aside class="aside aside-nav aside-career">
					<nav class="side-nav">
						<?php $aboutNav=[
							'Accounting',
							'link b',
							'link c'
						] ?>
						<?php for ($i=0; $i < sizeof($aboutNav); $i++) { ?>
							<a href="" class="<?= $i == 0 ? 'is-active' : ''; ?>"><?= $aboutNav[$i] ?></a>
						<?php } ?>
					</nav>
				</aside>
			</div>
			<div class="bzg_c" data-col="m1"></div>
			<div class="bzg_c" data-col="m7">
				<div class="section-page__content">
					<p class="h3"><strong>Job Vacancy Available</strong></p>
					<hr class="hr--style-one">
					<article class="section-page__article">
						<p class="text-red h2">Accounting</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat soluta laboriosam, possimus repellat esse quidem pariatur deleniti quis dicta ex odit explicabo quaerat nam maxime totam eos temporibus! Ducimus, quod!</p>
						<a href="career-detail.php" class="btn btn--green btn--rounded">APPLY</a>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'include/footer.php'; ?>