<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div id="site-top" class="parallax sr-only"></div>
<section class="section section-page-banner section--clear">
	<div class="parallax-window" data-parallax="scroll" data-image-src="assets/img/slide-1.jpg"></div>
</section>
<section class="section section-page-content">
	<div class="container">
		<h3 class="h3 section-page__title text-jumbo">Careers</h3>
		<div class="bzg">
			<div class="bzg_c">
				<div class="section-page__content">
					<div class="block">
						<div class="v-center v-center--spread block">
							<p class="h3"><strong>Accounting</strong></p>
							<a href="">Back</a>
						</div>
						<hr class="hr--style-one">
					</div>
				</div>
			</div>
			<div class="bzg_c" data-col="m6">
				<form action="" class="form--style-one form-validation">
					<div class="field-group">
					    <label for="" class="sr-only">Name</label>
					    <input type="text" name="" id="" placeholder="Name" class="form-input inputValidation" required>
					</div>
					<div class="field-group">
					    <label for="" class="sr-only">Email</label>
					    <input type="email" name="" id="" placeholder="Email" class="form-input inputValidation" required>
					</div>
					<div class="field-group">
					    <label for="" class="sr-only">Phone Number</label>
					    <input type="text" name="" id="" placeholder="Phone Number" class="form-input inputValidation" required>
					</div>
					<div class="field-group">
	                    <label for="" class="sr-only">Upload Resume</label>
		                <div class="field block">
		                    <input type="text" placeholder="Resume *doc" class="form-input" data-input-placeholder="resume" required>
		                </div>
		            	<div class="field">
		                    <input type="file" name="file" id="file" class="inputfile inputfile--grey inputfile--block" data-inputfile-target="resume"/>
		                    <label for="file">Upload A Resume or other doc</label>
		                </div>
	                </div>
					<button class="btn btn--rounded btn--green">SUBMIT</button>

				</form>
			</div>
		</div>
	</div>
</section>

<?php include 'include/footer.php'; ?>