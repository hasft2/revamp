<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div id="site-top" class="parallax sr-only"></div>
<section class="section section-page-banner section--clear">
	<div class="parallax-window" data-parallax="scroll" data-image-src="assets/img/slide-1.jpg"></div>
</section>
<section class="section section-page-content">
	<div class="container">
		<h3 class="h3 section-page__title text-jumbo">Services</h3>
		<div class="bzg">
			<div class="bzg_c" data-col="m4">
				<aside class="aside aside-nav aside-about">
					<nav class="side-nav">
						<?php $sideNav=[
							'Advertiser',
							'Affiliate'
						] ?>
						<?php for ($i=0; $i < sizeof($sideNav); $i++) { ?>
							<a href="" class="<?= $i == 0 ? 'is-active' : ''; ?>"><?= $sideNav[$i] ?></a>
						<?php } ?>
					</nav>
				</aside>
			</div>
			<div class="bzg_c" data-col="m1"></div>
			<div class="bzg_c" data-col="m7">
				<div class="section-page__content">
					<p class="h3"><strong>Advertiser</strong></p>
					<hr class="hr--style-one">
					<article class="section-page__article block">
						<div class="videoWrapper block">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/QARALafdWUI" frameborder="0" allowfullscreen></iframe>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur sapiente, eligendi atque officia perferendis, aut molestiae. Vero consequatur nesciunt, repellendus quo sequi quos sed aliquam, excepturi deserunt iste totam accusamus.</p>
					</article>
					<a href="" class="btn btn--green btn--rounded">Join Now</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'include/footer.php'; ?>