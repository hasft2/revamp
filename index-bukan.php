<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div class="on-screen textillate parallax sr-only"></div>
<section class="section section-home-slider section--clear">
	<div id="slider-home" class="slider slider--dots-abs-bottom" data-slick='{"arrows": false, "dots": true}'>
		<div class="slide__item">
			<div class="slide__img-bg slide__img-bg--panning" style="background-image:url(assets/img/slide-1.jpg)">
			</div>
			<div class="slide__caption slide__caption--animate slide__caption--bottom">
				<p class="text-jumbo"><strong>BLAST YOUR ADS GOING VIRAL.</strong></p>
				<a href="" class="btn btn--rounded btn--orange"><i class="fa fa-caret-right"></i> Watch Video</a>
			</div>
		</div>
		<div class="slide__item">
			<div class="slide__img-bg slide__img-bg--panning" style="background-image:url(assets/img/slide-1.jpg)">
			</div>
			<div class="slide__caption slide__caption--animate slide__caption--bottom">
				<p class="text-jumbo"><strong>2 BLAST YOUR ADS GOING VIRAL.</strong></p>
				<a href="" class="btn btn--rounded btn--orange"><i class="fa fa-caret-right"></i> Watch Video</a>
			</div>
		</div>
	</div>
	<div class="fake-slider"></div>
	<button class="btn btn--icon btn--circle btn--circle-outline btn--circle-outline--white custom-arrows custom-arrows-prev" data-target-slider="slider-home">
		<i class="fa fa-chevron-left"></i>
	</button>
	<button class="btn btn--icon btn--circle btn--circle-outline btn--circle-outline--white custom-arrows custom-arrows-next" data-target-slider="slider-home">
		<i class="fa fa-chevron-right"></i>
	</button>
</section>
<section class="section section-info">
	<div class="section-info__container container">
		<div class="info-tab tab tab--style-one">
		    <div class="tab-navs">
		        <a class="tab-nav text-center is-active" href="#tab1">
		        	<img src="assets/img/speaker.png" alt="" class="block">
		        	<p class="h3"><strong>Digital Ads Blaster</strong></p>
		        	<p>Sebar iklan Anda ke ribuan sampai jutaan pengguna gadget secara cepat dan viral melalui kekuatan media sosial.</p>
		        </a>
		        <a class="tab-nav text-center" href="#tab2">
		        	<img src="assets/img/target.png" alt="" class="block">
		        	<p class="h3"><strong>Smart targeted ads</strong></p>
		        	<p>Sebuah layanan yang mengidentifikasi, melacak dan menyebar video Anda ke penonton yang ditargetkan.</p>
		        </a>
		        <a class="tab-nav text-center" href="#tab3">
		        	<img src="assets/img/hand-money.png" alt="" class="block">
		        	<p class="h3"><strong>Watch and get paid</strong></p>
		        	<p>Adshare juga memberikan keuntungan lebih kepada orang-orang yang menonton, berkomentar dan berbagi iklan.</p>
		        </a>
		    </div>
		    <div class="tab-separator text-center">
			    <img src="assets/img/section-arrwos.png" alt="">	    	
		    </div>
		    <div class="tab-contents">
		        <div class="tab-content is-active" id="tab1">
		        	<div class="block">
		        		<div class="bzg">
		        			<div class="bzg_c" data-col="m6"></div>
		        			<div class="bzg_c" data-col="m6">
		        				<p class="h1 text-red header-font"><strong>How We Do</strong></p>
		        				<span class="extra-space"></span>
		        			</div>
		        			<div class="bzg_c" data-col="m6">
		        				<img src="assets/img/work.png" alt="" class="elem-animate" data-anim-js="come-in-left">	
		        			</div>
		        			<div class="bzg_c elem-animate" data-anim-js="come-in-right" data-col="m6">
			        			<p class="h1"><strong>Advertiser</strong></p>
			        			<p>Your goal is our first priority. Kami fokus pada dunia periklanan. Melalui platform dan ribuan afiliasi kami, kami dapat memberikan jalur terbaik untuk bisnis Anda dalam membantu iklan Anda hingga viral. Hubungi kami sekarang dan kami akan bekerja dengan Anda untuk mencapai tujuan bisnis Anda.</p>
			        			<div class="v-center">
			        				<a href="" class="btn btn--rounded btn--green">Join Now</a>
			        				<a href="" class="btn btn--icon text-blue">watch video <i class="fa fa-play-circle"></i></a>
			        			</div>
		        			</div>
		        		</div>
		        	</div>
		        	<div class="block">
		        		<div class="bzg">
		        			<div class="bzg_c" data-col="m6">
		        				<img src="assets/img/laptop.png" alt="" class="elem-animate" data-anim-js="come-in-left">	
		        			</div>
		        			<div class="bzg_c elem-animate" data-anim-js="come-in-right" data-col="m6">
			        			<p class="h1"><strong>Affiliate</strong></p>
			        			<p>Adshare cooperate with netizens who are active in social media such as facebook and twitter. Ya, kami mengundang Anda untuk menjadi mitra kerja kami sebagai afiliasi aktif. Kabar baiknya adalah kami memiliki banyak iklan yang menunggu Anda. Watch the ads and get paid.</p>
			        			<div class="v-center">
			        				<a href="" class="btn btn--rounded btn--green">Join Now</a>
			        				<a href="" class="btn btn--icon text-blue">watch video <i class="fa fa-play-circle"></i></a>
			        			</div>
		        			</div>
		        		</div>
		        	</div>
		        </div>
		        <div class="tab-content" id="tab2">
		        	dasdsadasdsad 2
		        </div>
		        <div class="tab-content" id="tab3">
		        	dasdsadasdsad 3
		        </div>
		    </div>
		</div>
	</div>
</section>
<section class="section section-all-info section--clear">
	<div class="accordion-group accordion-group--style-one accordion-group--hide-after">
	    <section class="section accordion">
	        <header class="accordion__title bg-grey h3">
	        	<div class="container v-center">
	        		<div class="btn-chevron">
						<button class="btn btn--icon btn--circle">
	            			<i class="fa fa-chevron-down" aria-hidden="true"></i>
	            		</button>
	        		</div>
					<div class="header-font-medium">
						<p class="h2 elem-animate" data-in-effect="fadeInLeft" data-anim-js="tlt">
							A brand is no longer what we tell the consumer it is -- it is what consumers tell each other it is.
						</p>
						<small>Scott Cook</small>
					</div>
				</div>
	        </header>
	        <div class="accordion__content">
	        	<div class="custom-dots__container">
	            	<?php include 'include/section-affiliate-info.php'; ?>
	            	<div class="custom-dots clearfix">
	            		<div class="tooltip-container">
		            		<button class="btn btn--icon btn--circle btn--circle-2x is-active" data-goto="0">
		            			<i class="fi fa-2x flaticon-income" aria-hidden="true"></i>
		            		</button>
		            		<div class="tooltip tooltip--wide">
		            			<div class="tooltip__content">
		            				<span class="h2"><strong>Lifetime Passive Income.</strong></span>
		            				<p>Maksimalkan potensi penghasilan dari iklan tanpa biaya dengan mudah.</p>
		            			</div>
		            		</div>
	            		</div>
	            		<div class="tooltip-container">
		            		<button class="btn btn--icon btn--circle btn--circle-2x" data-goto="1">
		            			<i class="fi fa-2x flaticon-sofa" aria-hidden="true"></i>
		            		</button>
		            		<div class="tooltip tooltip--wide">
		            			<div class="tooltip__content">
		            				<span class="h2"><strong>High Flexibility</strong></span>
		            				<p>Being an affiliate means you are your own boss, Anda dapat menonton iklan kapan saja dan di mana saja.</p>
		            			</div>
		            		</div>
	            		</div>
	            		<div class="tooltip-container">
		            		<button class="btn btn--icon btn--circle btn--circle-2x" data-goto="2">
		            			<i class="fi fa-2x flaticon-atm" aria-hidden="true"></i>
		            		</button>
		            		<div class="tooltip tooltip--wide">
		            			<div class="tooltip__content">
		            				<span class="h2"><strong>Easy Access</strong></span>
		            				<p>Anda dapat menarik rekening dana Anda dengan mudah.</p>
		            			</div>
		            		</div>
	            		</div>
	            	</div>
	        	</div>
	        </div>
	    </section>
	    <section class="section accordion">
	        <header class="accordion__title bg-grey h3">
	        	<div class="container v-center">
					<div>
						<p class="h2 text-blue elem-animate" data-in-effect="fadeInLeft" data-anim-js="tlt">
							I don't think consumers are frustrated with advertising, they are frustrated with badly targeted ads.
						</p>
						<small>Scott Cook</small>
					</div>
					<div class="btn-chevron">
						<button class="btn btn--icon btn--circle">
	            			<i class="fa fa-chevron-down" aria-hidden="true"></i>
	            		</button>
	        		</div>
				</div>
	        </header>
	        <div class="accordion__content">
	        	<div class="custom-dots__container">
	            	<?php include 'include/section-advertiser-info.php'; ?>
		            <div class="custom-dots clearfix">
	            		<div class="tooltip-container">
		            		<button class="btn btn--icon btn--circle btn--circle-2x is-active" data-goto="0">
		            			<i class="fi fa-2x flaticon-income" aria-hidden="true"></i>
		            		</button>
		            		<div class="tooltip tooltip--wide">
		            			<div class="tooltip__content">
		            				<span class="h2"><strong>Beyond Viewablity.</strong></span>
		            				<p>Adshare akan mencatat jumlah penonton yang melihat iklan Anda secara luas beserta rating keamanan.</p>
		            			</div>
		            		</div>
	            		</div>
	            		<div class="tooltip-container">
		            		<button class="btn btn--icon btn--circle btn--circle-2x" data-goto="0">
		            			<i class="fi fa-2x flaticon-sofa" aria-hidden="true"></i>
		            		</button>
		            		<div class="tooltip tooltip--wide">
		            			<div class="tooltip__content">
		            				<span class="h2"><strong>High Flexibility</strong></span>
		            				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum, dolores?</p>
		            			</div>
		            		</div>
	            		</div>
	            		<div class="tooltip-container">
		            		<button class="btn btn--icon btn--circle btn--circle-2x" data-goto="0">
		            			<i class="fi fa-2x flaticon-atm" aria-hidden="true"></i>
		            		</button>
		            		<div class="tooltip tooltip--wide">
		            			<div class="tooltip__content">
		            				<span class="h2"><strong>High Flexibility</strong></span>
		            				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum, dolores?</p>
		            			</div>
		            		</div>
	            		</div>
		            </div>
	        	</div>
	        </div>
	    </section>
	</div>
</section>
<section class="section section-how-work">
	<div class="section-how-work__container container text-center">
		<p class="h2 section__title"><strong>How We Work</strong></p>
		<div class="circle-list clearfix">
			<div class="circle bg-orange">
				<div class="circle__content">
					<img src="assets/img/watch-icon.png" class="block" alt="">
					<p class="h3 circle__content__title">Watch</p>
					<p class="circle__content__desc">Take a look at our smart
targeted ads.</p>
				</div>
			</div>
			<i class="fa fa-chevron-right fa-2x"></i>
			<div class="circle bg-blue">
				<div class="circle__content">
					<img src="assets/img/share-icon.png" class="block" alt="">
					<p class="h3 circle__content__title">Share</p>
					<p class="circle__content__desc">Take a look at our smart
targeted ads.</p>
				</div>
			</div>
			<i class="fa fa-chevron-right fa-2x"></i>
			<div class="circle bg-green">
				<div class="circle__content">
					<img src="assets/img/get-paid-icon.png" class="block" alt="">
					<p class="h3 circle__content__title">Get Paid</p>
					<p class="circle__content__desc">Take a look at our smart
targeted ads.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section section-innovative">
	<img src="assets/img/hand-and-phone.jpg" alt="" class="hand-and-phone-img">
	<div class="container text-center">
		<img src="assets/img/mac.png" alt="" class="mac-img elem-animate block" data-anim-js="come-in">
		<p class="h2 section__title"><strong>Innovative services and solutions</strong> start here.</p>
		<p>Adshare telah membangun tim eksekutif terlatih dengan lebih dari 12 tahun pengalaman dalam bidang kreatif, media, digital, bisnis dan teknik perangkat lunak. Berkomitmen dengan visi yang sama, Adshare terus melibatkan advertiser dengan menciptakan solusi cerdas untuk menjawab kebutuhan dunia advertising saat ini.</p>
	</div>
</section>
<hr class="hr--style-one">
<section class="section section-running-ads text-center">
	<p class="h2 section__title"><strong>Running Ads</strong></p>
	<p>These ads are waiting for you. <a href=""><strong>Join now!</strong></a></p>
	<div id="slider-running-ads" class="slider slider-carousel slider-carousel--center thumbs-style-one" data-slick='{"centerMode": true, "arrows": false, "focusOnSelect": true, "centerPadding": "60px", "dots": true}'>
		<?php for ($i=0; $i < 7; $i++) { ?> 
			<div class="thumb">
                <div class="thumb__link" href="">
                    <div class="thumb__img" style="background-image: url(assets/img/sample-img-vid-thumb.jpg)">
                    	<button class="btn btn--icon thumb-icon-play">
                            <i class="icon fi flaticon-play h1"></i>
                        </button>
                        <span class="duration">00:30</span>
                    </div>
                    <div class="thumb__info">
                        <a href="" class="thumb__title flex-container">
                            <span>Thumb title maskdmsakd dmaskdmaskd daskdmaskdmsad dsamkdmas</span>
                        </a>
                        <div class="thumb__info__footer">
                            <small class="thumb__info__brand">by Buavita.co.id</small>
                            <small class="thumb__info__total-played text-red">0 played</small>
                        </div>
                    </div>
                </div>
            </div>
		<?php } ?>
	</div>
</section>
<hr class="hr--style-one">
<section class="section section-partner">
	<div class="container text-center">
		<p class="h2 section__title"><strong>Our Clients</strong></p>
		<div class="v-center v-center--around block">
			<?php for ($i=0; $i < 5; $i++) { ?>
				<a href="">
					<img src="assets/img/partner-1.jpg" alt="">
				</a>
			<?php } ?>
		</div>
	</div>
</section>
<section class="section section-contact section--clear">
	<div class="bzg">
		<div class="bzg_c" data-col="m6">
			<img src="assets/img/building.jpg" alt="">		
		</div>
		<div class="bzg_c" data-col="m6">
			<div class="contact-block">
				<p class="h2 section__title"><strong>Contact Us</strong></p>
				<p>If you have any questions, we'd love to talk to you.</p>
				<form action="" class="form--style-one">
					<?php include 'include/form-contact.php'; ?>
				</form>
			</div>
		</div>
	</div>
</section>

<?php include 'include/footer.php'; ?>