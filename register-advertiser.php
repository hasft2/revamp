<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div class="container">
	<span class="extra-space-5x"></span>
	<section class="section section--clear">
		<div class="block text-center">
			<h1>Selamat</h1>
			<p>Anda telah berhasil bergabung di AdShare dan verifikasi email Anda telah berhasil. Untuk melanjutkan proses registrasi, Anda diharuskan melengkapi data personalisasi member. Kelengkapan data menjadi suatu proses yang penting guna melengkapi syarat utama menjadi Affiliate AdShare. Semua data pribadi Anda akan aman dan kami rahasiakan dari publik.</p>
		</div>
		<div class="extra-space"></div>
		<p class="h2 text-center"><strong>Formulir Data Diri ADVERTISER ADSHARE</strong></p>
		<p class="text-red text-center">Private & Confidential</p>
		<span class="extra-space"></span>
		<form action="" class="form--style-one form-validation">
			<?php include 'include/form-advertiser.php'; ?>
		</form>
	</section>
	<div class="extra-space"></div>
	<div class="extra-space"></div>
</div>
<?php include 'include/footer.php'; ?>