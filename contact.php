<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div id="site-top" class="parallax sr-only"></div>
<section class="section section-page-banner section--clear">
	<div class="parallax-window" data-parallax="scroll" data-image-src="assets/img/slide-1.jpg"></div>
</section>
<section class="section section-page-content">
	<div class="container">
		<h3 class="h3 section-page__title text-jumbo">Contact Us</h3>
		<div class="bzg">
			<div class="bzg_c" data-col="m5">
				<div class="section-page__content">
					<p class="h2"><strong>Contact Us</strong></p>
					<hr class="hr--style-one">
					<article class="section-page__article">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio quidem laudantium quos perspiciatis impedit voluptates a atque, numquam, excepturi animi, inventore incidunt minima ab commodi iste eum alias! Aperiam, accusantium.</p>
					</article>
					<form action="" class="form--style-one">
						<div class="field-block">
							<label for="" class="sr-only">Name</label>
							<input type="text" class="form-input" placeholder="Name">
						</div>
						<div class="field-block">
							<label for="" class="sr-only">email</label>
							<input type="email" class="form-input" placeholder="email">
						</div>
						<div class="field-block">
							<label for="" class="sr-only">Message</label>
							<textarea name="" id="" cols="30" rows="10" placeholder="Message" class="form-input"></textarea>
						</div>
						<div class="field-block">
							<button class="btn btn--green btn--rounded">Submit</button>
						</div>
					</form>
				</div>
			</div>
			<div class="bzg_c" data-col="m2"></div>
			<div class="bzg_c" data-col="m5">
				<div class="section-page__content">
					<p class="h2"><strong>Office</strong></p>
					<hr class="hr--style-one">
					<article class="section-page__article">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat soluta laboriosam, possimus repellat esse quidem pariatur deleniti quis dicta ex odit explicabo quaerat nam maxime totam eos temporibus! Ducimus, quod!</p>

						<p class="h3"><strong>Address</strong></p>
						<p>Lorem ipsum<br>dolor sit amet.</p>

						<p class="h3"><strong>Phones</strong></p>
						<p>Phone: +62 839213 21<br>Cell: +62 3123 312312312</p>

						<p class="h3"><strong>Emails</strong></p>
						<p><a href="" class="text-red">info@adshare.id</a></p>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'include/footer.php'; ?>