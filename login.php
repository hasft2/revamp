<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div class="container">
	<span class="extra-space-5x"></span>
	<section class="section section--clear">
		<div class="small-spot">
			<p class="h2 text-center"><strong>Login</strong></p>
			<form action="" class="form--style-one form-validation">
				<?php include 'include/form-login.php'; ?>
			</form>
		</div>
	</section>
	<div class="extra-space"></div>
	<div class="extra-space"></div>
</div>
<?php include 'include/footer.php'; ?>