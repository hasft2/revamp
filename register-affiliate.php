<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div class="container">
	<span class="extra-space-5x"></span>
	<section class="section section--clear">
		<div class="block text-center">
			<h1>Selamat</h1>
			<p>Anda telah berhasil bergabung di AdShare dan verifikasi email Anda telah berhasil. Untuk melanjutkan proses registrasi, Anda diharuskan melengkapi data personalisasi member. Kelengkapan data menjadi suatu proses yang penting guna melengkapi syarat utama menjadi Affiliate AdShare. Semua data pribadi Anda akan aman dan kami rahasiakan dari publik.</p>
			<a href="#" class="pop-trigger" data-target="#test-pop">Test Pop</a>
		</div>
		<div class="extra-space"></div>
		<p class="h2 text-center"><strong>Formulir Data Diri AFFILIATES ADSHARE</strong></p>
		<p class="text-red text-center">Private & Confidential</p>
		<span class="extra-space"></span>
		<form action="" class="form--style-one form-validation">
			<?php include 'include/form-affiliate.php'; ?>
		</form>
	</section>
	<div class="extra-space"></div>
	<div class="extra-space"></div>
</div>
<div class="popup" id="test-pop">
    <button class="btn btn-close btn--icon btn--icon--clear btn-corner-right btn-close--red">
        <i class="fa fa-times-circle"></i>
    </button>
    <div class="popup__content popup-sendlove text-center">
        <p class="popup__content__title text-uppercase"><strong>Test</strong></p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum in nostrum quis ad quas atque, perferendis sed vel eligendi impedit temporibus consequuntur esse voluptatem! Facilis atque, enim labore iusto nostrum.</p>
        
    </div>
</div>
<?php include 'include/footer.php'; ?>