<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div class="container">
	<div class="extra-space-4x"></div>
	<div class="bzg">
		<div class="bzg_c" data-col="m2">
			<img src="assets/img/banner-adshare.jpg" alt="" class="ads">
		</div>
		<div class="bzg_c" data-col="m8">
			<form action="">
				<div class="video-wrapper">
					<div class="question-step thankyou">
                         <div class="step" data-step="1">
                            <div class="step__body text-center card__container">
                                <div class="thankyou__text">
                                    <h3>Tonton Terus Video di Adshare,</h3>
                                    <p>Kumpulkan Kreditnya di setiap video yang kamu tonton<br class="show-medium">dan dapatkan uang tunai</p>
                                    <button class="btn btn--style-one btn--style-dashboard-orange">Buat Akun Sekarang</button>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="js-media-player plyr">
		                <video poster="http://placehold.it/700x395" controls>
		                    <!-- Video files -->
		                    <source src="assets/video/big_buck_bunny.mp4" type="video/mp4">
		                    <source src="assets/video/big_buck_bunny.webm" type="video/webm">

		                    <!-- Fallback for browsers that don't support the <video> element -->
		                    <a href="assets/video/big_buck_bunny.mp4">Download</a>
		                </video>       
		            </div>
				</div>
			</form>
            <div class="card">
	            <div class="bzg">
		            <div class="bzg_c" data-col="m6">
		            	<p class="h2">Buavita 2go Rasa APel</p>
		            	<div class="flexblock">
                            <div class="user__img user__img--small" style="background-image:url('http://placehold.it/45x45')"></div>
                            <span class="text-right">by Buavita.co.id</span>
                        </div>
		            </div>
		            <div class="bzg_c text-right" data-col="m6">
		            	<p class="h2 text-red">13.876</p>
		            	<a href="" class="btn btn--style-one">
		            		<i class="fa fa-check"></i>
		            		Subscribe Brand
		            	</a>
		            </div>
	            </div>
            </div>
            <hr class="hr--style-one">
            <div class="card">
            	<p class="h2">Waiting Ads To Watch</p>
            	<div class="thumbs thumbs--style-one">
	            	<?php for ($i=0; $i < 8; $i++) { ?>
	            		<div class="thumb">
			                <div>
			                    <div class="thumb__img" style="background-image: url(assets/img/sample-img-vid-thumb.jpg)">
			                    	<button class="btn btn--icon thumb-icon-play">
			                            <i class="icon fi flaticon-play h1"></i>
			                        </button>
			                        <span class="duration">00:30</span>
			                    </div>
			                    <div class="thumb__info">
			                        <a href="" class="thumb__title text-green">
			                            <span>Thumb title maskdmsakd dmaskdmaskd daskdmaskdmsad dsamkdmas</span>
			                        </a>
			                        <div class="thumb__info__footer">
			                            <small class="thumb__info__brand">by Buavita.co.id</small>
			                            <small class="thumb__info__total-played text-red">0 played</small>
			                        </div>
			                    </div>
			                </div>
			            </div>
	            	<?php } ?>
            	</div>
            </div>
		</div>
		<div class="bzg_c" data-col="m2">
			<img src="assets/img/banner-adshare.jpg" alt="" class="ads">
		</div>
	</div>	
</div>


<?php include 'include/footer.php'; ?>