<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div class="container">
	<span class="extra-space-5x"></span>
	<section class="section section--clear">
		<div class="small-spot">
			
			<form action="" class="form--style-one form-validation">
				<?php include 'include/form-signup.php'; ?>
			</form>
		</div>
	</section>
	<div class="extra-space"></div>
	<div class="extra-space"></div>
</div>
<?php include 'include/footer.php'; ?>