<button class="btn btn--rounded btn--blue btn--block block"><i class="fa fa-facebook"></i> Facebook</button>
<hr>
<div class="field-group">
    <label for="" class="sr-only">User Name</label>
    <input type="text" name="" id="" placeholder="User Name" class="form-input inputValidation" required>
</div>
<div class="field-group">
    <label for="" class="sr-only">Email</label>
    <input type="text" name="" id="" placeholder="email" class="form-input inputValidation" required>
</div>
<div class="field-group">
    <label for="" class="sr-only">Password</label>
    <input type="password" name="" id="" placeholder="password" class="form-input inputValidation" required>
</div>
<div class="field-group">
    <label for="" class="sr-only">Confirm Password</label>
    <input type="password" name="" id="" placeholder="confirm password" class="form-input inputValidation" required>
</div>
<small class="text-center">By Clicking on Sign up, you agree to <a href="" class="text-green">Adshare's terms & conditions</a> and <a href="" class="text-green">privacy policy</a></small>
<span class="extra-space"></span>
<button class="btn btn--rounded btn--green btn--block block">Signup</button>
<p class="text-center">Already Have an Account? <a href="" class="text-green">Login</a></p>
