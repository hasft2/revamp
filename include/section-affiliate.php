<header class="accordion__title accordion__title--no-arrow bg-grey h3">
	<div class="container v-center">
		<div class="btn-chevron">
			<a class="btn btn--icon btn--circle" href="#head-section-affiliate">
    			<i class="fa fa-chevron-down" aria-hidden="true"></i>
    		</a>
		</div>
		<div class="header-font-medium">
			<p class="h2">
				A brand is no longer what we tell the consumer it is -- it is what consumers tell each other it is.
			</p>
			<small>Scott Cook</small>
		</div>
	</div>
</header>
<header id="head-section-affiliate" class="slider__item__header text-center bg-orange header-font-medium">
	<p class="h3 slider__item__header__title">All you need is internet connectivity</p>
	<p class="h2">Be Our Affiliate</p>
	<i class="fa fa-2x fa-caret-down caret-down text-orange" aria-hidden="true"></i>
</header>
<div class="parallax-container">
	<div id="af1" class="slide__img-bg paraxify is-active" style="background-image: url(assets/img/bg-w-1.jpg);"></div>
	<div id="af2" class="slide__img-bg paraxify" style="background-image: url(assets/img/bg-w-2.jpg);"></div>
	<div id="af3" class="slide__img-bg paraxify" style="background-image: url(assets/img/bg-woman.jpg);"></div>
	<div class="parallax-dom-elem">
		<div class="container">
			<div class="slide__caption slide__caption--white text-center">
				<p class="h2">Affiliate Benefits</p>
				<span class="bar bar--orange"></span>
			</div>
			<div class="hidden-medium">
				<div class="slider slider-custom-dots">
					<?php for ($i=0; $i < 3; $i++) { ?>
						<div class="slide__item text-center">
							<span class="h2"><strong>High Flexibility</strong></span>
							<p>Being an affiliate means you are your own boss, Anda dapat menonton iklan kapan saja dan di mana saja.
						</div>
					<?php } ?>
				</div>
			</div>
			<?php include 'include/custom-dots.php'; ?>
		</div>
	</div>
</div>
