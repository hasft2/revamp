<header class="accordion__title accordion__title--no-arrow bg-grey h3">
	<div class="container v-center">
		<div>
			<p class="h2 text-blue">
				I don't think consumers are frustrated with advertising, they are frustrated with badly targeted ads.
			</p>
			<small>Scott Cook</small>
		</div>
		<div class="btn-chevron">
			<a class="btn btn--icon btn--circle" href="#head-section-how-work">
    			<i class="fa fa-chevron-down" aria-hidden="true"></i>
    		</a>
		</div>
	</div>
</header>
<header id="head-section-how-work" class="slider__item__header text-center bg-green header-font-medium">
	<p class="h3 slider__item__header__title">All you need is internet connectivity</p>
	<p class="h2">Be Advertiser</p>
	<i class="fa fa-2x fa-caret-down caret-down text-green" aria-hidden="true"></i>
</header>
<div class="parallax-container">
	<!--<div class="parallax-window" data-natural-width="1371" data-natural-height="584">
		<div class="parallax-slider">
			<img src="assets/img/bg-concert.jpg" sizes="100vw">
		</div>
	</div>-->
	<div id="ad1" class="slide__img-bg paraxify is-active" style="background-image: url(assets/img/bg-concert0.jpg);"></div>
	<div id="ad2" class="slide__img-bg paraxify" style="background-image: url(assets/img/bg-concert1.jpg);"></div>
	<div id="ad3" class="slide__img-bg paraxify" style="background-image: url(assets/img/bg-concert.jpg);"></div>
	<div class="parallax-dom-elem">
		<div class="container">
			<div class="slide__caption slide__caption--white text-center">
				<p class="h2">Advertiser Benefits</p>
				<span class="bar bar--green"></span>
			</div>
			<?php include 'include/custom-dots2.php'; ?>
		</div>
	</div>
</div>