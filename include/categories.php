<div class="module" data-module="categories">
	<p class="module-title h3"><strong>Categories</strong></p>
	<hr class="hr--style-one block">
	<?php $categories=[
		'advertising',
		'lifestyle',
		'Brand'
	] ?>
	<div class="accordion-group accordion--regular-list" data-toggle="single">
		<?php for ($i=0; $i < sizeof($categories); $i++) { ?>
			<section class="accordion">
				<span class="accordion__title"><?= $categories[$i] ?> (14)</span>
				<div class="accordion__content">
					...
				</div>
			</section>
		<?php } ?>
	</div>
</div>