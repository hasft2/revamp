<div class="custom-dots clearfix">
	<div class="tooltip-container">
		<button class="btn btn--icon btn--circle btn--circle-2x is-active" data-target="#ad1">
			<i class="fi fa-2x flaticon-income" aria-hidden="true"></i>
		</button>
		<div class="tooltip tooltip--wide">
			<div class="tooltip__content">
				<span class="h2"><strong>Beyond Viewability.</strong></span>
				<p>Adshare akan mencatat jumlah penonton yang melihat iklan Anda secara luas beserta rating keamanan.</p>
			</div>
		</div>
	</div>
	<div class="tooltip-container">
		<button class="btn btn--icon btn--circle btn--circle-2x" data-target="#ad2">
			<i class="fi fa-2x flaticon-sofa" aria-hidden="true"></i>
		</button>
		<div class="tooltip tooltip--wide">
			<div class="tooltip__content">
				<span class="h2"><strong>Advanced Statistics</strong></span>
				<p>Being an affiliate means you are your own boss, Anda dapat menonton iklan kapan saja dan di mana saja.</p>
			</div>
		</div>
	</div>
	<div class="tooltip-container">
		<button class="btn btn--icon btn--circle btn--circle-2x" data-target="#ad3">
			<i class="fi fa-2x flaticon-atm" aria-hidden="true"></i>
		</button>
		<div class="tooltip tooltip--wide">
			<div class="tooltip__content">
				<span class="h2"><strong>Easy Access</strong></span>
				<p>Anda dapat menarik rekening dana Anda dengan mudah.</p>
			</div>
		</div>
	</div>
</div>