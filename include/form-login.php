<div class="field-group">
    <label for="" class="sr-only">Email</label>
    <input type="text" name="" id="" placeholder="email" class="form-input inputValidation" required>
</div>
<div class="field-group">
    <label for="" class="sr-only">Password</label>
    <input type="password" name="" id="" placeholder="password" class="form-input inputValidation" required>
</div>
<div class="field-group">
    <input type="checkbox" id="forget_pass">
    <label for="forget_pass">forget password</label>
</div>
<button class="btn btn--rounded btn--red block">Login</button>
