<header class="site-header header-compact">
	<div class="site-header__container container v-center v-center--spread">
		<div class="header__left v-center v-center--spread">
			<a href="index.php" class="logo header__left">
				<img src="assets/img/logo.png" alt="">
			</a>
			<button class="btn btn--icon btn--icon--clear menu-trigger hide-desktop">
				<i class="fa fa-bars" aria-hidden="true"></i>
			</button>
		</div>
		<div class="utils header__right">
			<div class="user-helper v-center block">
				<div class="btn btn--icon btn--icon--clear contact user-helper__item">
					<i class="fa fa-phone" aria-hidden="true"></i>
					+62 821 1111 3998
				</div>
				<div class="drop-down">
					<a href="#" class="btn btn--icon-w-text btn--grey btn--rounded drop-down__anchor">
						<span>Indonesia</span> <i class="fa fa-sort"></i>
					</a>
					<div class="drop-down__pop">
		                <div class="drop-down__pop__inner">
		                    <i class="fa fa-caret-up drop-down__pop__arrow"></i>
		                    <a href="#" class="drop-down__pop__anchor">English</a>
		                    <a href="#" class="drop-down__pop__anchor">Indonesia</a>
		                </div>
		            </div>
				</div>
				<div class="drop-down">
					<a href="#" class="btn btn--icon btn--icon--clear user-helper__item text-red drop-down__anchor">
						<i class="fa fa-user"></i>
						Sign Up
					</a>
					<div class="drop-down__pop">
		                <div class="drop-down__pop__inner">
		                    <i class="fa fa-caret-up drop-down__pop__arrow"></i>
		                    <a href="register-advertiser.php" class="drop-down__pop__anchor">Advertiser</a>
		                    <a href="register-affiliate.php" class="drop-down__pop__anchor">Affiliate</a>
		                </div>
		            </div>
				</div>
				<a href="login.php" class="btn btn--icon btn--icon--clear user-helper__item text-red">
					<i class="fa fa-sign-in"></i>
					Login
				</a>
			</div>
			<nav class="nav header-nav">
				<ul class="nav-list list-nostyle">
					<li>
						<a href="login.php" class="btn btn--rounded"><strong><i class="fa fa-sign-in"></i>
					Login</strong></a>
					</li>
					<li>
						<a href="#" class="btn btn--rounded pop-trigger" data-target="#signup-pop"><strong><i class="fa fa-user"></i>
						Sign Up</strong></a>
					</li>
					<li><a href="index.php" class="btn btn--rounded is-active"><strong>home</strong></a></li>
					<li><a href="about.php" class="btn btn--rounded"><strong>about</strong></a></li>
					<li><a href="services.php" class="btn btn--rounded"><strong>services</strong></a></li>
					<li><a href="blog.php" class="btn btn--rounded"><strong>blog</strong></a></li>
					<li><a href="career.php" class="btn btn--rounded"><strong>career</strong></a></li>
					<li><a href="contact.php" class="btn btn--rounded"><strong>contact</strong></a></li>
				</ul>
			</nav>
		</div>
	</div>
</header>