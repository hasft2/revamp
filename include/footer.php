	<div class="popup-overlay"></div>
	<div class="popup" id="signup-pop">
	    <button class="btn btn-close btn--icon btn--icon--clear btn-corner-right btn-close--red">
	        <i class="fa fa-times-circle"></i>
	    </button>
	    <div class="popup__content popup-sendlove text-center">
	        <a href="" class="btn btn--rounded btn--red btn--block block">Affiliate</a>
	        <a href="" class="btn btn--rounded btn--red btn--block">Advertiser</a>
	    </div>
	</div>
	<footer class="site-footer">
		<div class="site-footer__top container section show-desktop">
			<div class="v-center v-center--spread">
				<div class="site-map">
					<ul class="list-inline list-nostyle">
						<li><a href="" class="ss">about</a></li>
						<li><a href="" class="ss">services</a></li>
						<li><a href="" class="ss">blog</a></li>
						<li><a href="" class="ss">terms & conditions</a></li>
						<li><a href="" class="ss">contact</a></li>
					</ul>
				</div>
				<div>
					<a class="btn btn--icon btn--circle btn--circle-2x btn--circle-outline btn-go-to-top" href="#site-top">
						<i class="fa fa-2x fa-chevron-up"></i>
					</a>
				</div>
				<?php include 'include/social-media.php'; ?>
			</div>
		</div>
		<hr class="hr--style-one">
		<div class="site-footer__bottom section section--half section container">
			<div class="v-center v-center--spread">
				<span>©2016 <em class="text-red">Adshare.</em> All Rights Reserved.</span>
				<div>
					<a href="" class="text-red">
						info@adshare.id
					</a>
					<span>+62 31243 5234</span>
				</div>
			</div>
		</div>
	</footer>
	<script>window.myPrefix = '';</script>
    <script src="assets/js/main.min.js"></script>
</body>
</html>