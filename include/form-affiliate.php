<div class="bzg">
	<div class="bzg_c" data-col="m6">
		<div class="field-group">
            <label for="" class="">NIK / No. KTP</label>
            <input type="text" placeholder="NIK / No. KTP Anda" class="form-input" required>
        </div>
        <div class="field__upload field-group bzg">
            <label for="" class="bzg_c">Upload Image KTP</label>
            <div class="bzg_c" data-col="m8">
                <div class="field">
                    <input type="text" placeholder="Foto Ktp" class="form-input" data-input-placeholder="ktp" required>
                </div>
            </div>
            <div class="bzg_c" data-col="m4">
            	<div class="field">
                    <input type="file" name="file" id="file" class="inputfile" data-inputfile-target="ktp"/>
                    <label for="file">Browse...</label>
                </div>
            </div>
        </div>
        <div class="field-group">
            <label for="" class="">Nama Lengkap</label>
            <input type="text" placeholder="Nama Sesuai KTP" class="form-input" required>
        </div>
        <div class="field-group">
            <label for="" class="">Tempat / Tgl Lahir</label>
            <div class="bzg">
                <div class="bzg_c" data-col="m6">
                    <input type="text" placeholder="Kota Kelahiran" class="form-input" required>
                </div>
                <div class="bzg_c" data-col="m6">
                    <input id="datepicker" type="text" placeholder="ddmmyyyy" class="form-input date-picker" required>
                </div>
            </div>
        </div>
        <div class="field-group">
            <label for="" class="">Alamat Domisili</label>
            <textarea id="" class="form-input" required placeholder="Alamat Lengkap domisili anda" name="" cols="30" rows="10"></textarea>
        </div>
        <div class="field-group">
            <label for="" class="">No. HP</label>
            <input type="text" placeholder="Nomor telepon selular anda" class="form-input" required>
        </div>
        <div class="field-group">
            <label for="" class="">Agama</label>
            <input type="text" placeholder="Agama" class="form-input" required>
        </div>
        <div class="field-group">
            <label for="" class="">Email (aktif)</label>
            <input type="text" placeholder="Alamat Email Anda yang aktif" class="form-input" required>
        </div>
	</div>
	<div class="bzg_c" data-col="m6">
		 <div class="field-group">
            <label for="" class="">Akun Facebook Anda</label>
            <input type="text" placeholder="Akun Facebook Anda" class="form-input" required>
        </div>
        <div class="field-group">
            <label for="" class="">Akun Twitter Anda</label>
            <input type="text" placeholder="Akun Twitter Anda" class="form-input" required>
        </div>
        <div class="field-group">
            <span class="label">Status Pekerjaan</span>
            <div class="flex-container flex-container--medium flex-container--two-col">
                <div>
                    <div class="check-custom-odd">
                        <input type="radio" id="nokip">
                        <label for="nokip">Nokip</label>
                    </div>
                    <div class="check-custom-odd">
                        <input type="radio" id="nokip">
                        <label for="nokip">Nokip</label>
                    </div>
                </div>
                <div>
                    <div class="check-custom-odd">
                        <input type="radio" id="beer">
                        <label for="beer">Beer</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="field-group">
            <span class="label">Penghasilan Per Bulan</span>
            <div class="flex-container flex-container--medium flex-container--two-col">
                <div>
                    <div class="check-custom">
                        <input type="radio" id="gitar">
                        <label for="gitar">Gitar</label>
                    </div>
                </div>
                <div>
                    <div class="check-custom">
                        <input type="radio" id="beer">
                        <label for="beer">Beer</label>
                    </div>
                </div>
            </div>
        </div>
        <fieldset>
            <div class="fieldset__title">
                <hr class="">
                <span>Bank Details</span>
            </div>
            <div class="field-group">
                <label for="" class="">Nama Bank</label>
                <input type="text" placeholder="Nama Bank" class="form-input" required>
            </div>
            <div class="field-group">
                <label for="" class="">Cabang Bank</label>
                <input type="text" placeholder="Cabang Bank" class="form-input" required>
            </div>
            <div class="field-group">
                <label for="" class="">No. Rekening Bank</label>
                <input type="text" placeholder="Nomor Rekening Bank" class="form-input" required>
            </div>
        </fieldset>
	</div>
	<div class="bzg_c block">
		<div class="fied-group check-custom">
			<input type="checkbox">
			<label for="">sdasdsad</label>
		</div>
		<div class="fied-group check-custom">
			<input type="checkbox">
			<label for="">sdasdsad</label>
		</div>
		<div class="fied-group check-custom">
			<input type="checkbox">
			<label for="">sdasdsad</label>
		</div>
	</div>
</div>
<button class="btn btn--rounded btn--red">Sign Up</button>
