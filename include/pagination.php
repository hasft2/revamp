<div class="pagination-container v-center v-center--spread pagination--style-one">
	<a href="">
		<i class="fa fa-chevron-left"></i>
	</a>
	<ul class="pagination">
		<li><a href="" class="active"><strong>1</strong></a></li>
		<li><a href=""><strong>2</strong></a></li>
	</ul>
	<a href="">
		<i class="fa fa-chevron-right"></i>
	</a>
</div>