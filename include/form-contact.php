<div class="bzg">
    <div class="bzg_c" data-col="m6">
        <div class="field-group">
            <label for="" class="sr-only">Name</label>
            <input type="text" name="" id="" placeholder="Name*" class="form-input" required>
        </div>
    </div>
    <div class="bzg_c" data-col="m6">
        <div class="field-group">
            <label for="" class="sr-only">Email</label>
            <input type="text" name="" id="" placeholder="Email*" class="form-input" required>
        </div>
    </div>
    <div class="bzg_c">
        <div class="field-group">
            <label for="" class="sr-only">Subject</label>
            <input type="text" name="" id="" placeholder="Subject*" class="form-input" required>
        </div>
    </div>
    <div class="bzg_c">
        <div class="field-group">
        	<label for="" class="sr-only">Message</label>
        	<textarea name="" id="" cols="30" rows="10" class="form-input" placeholder="Message"></textarea>
        </div>
    </div>
</div>
<div class="captcha"></div>
<div class="field-group block">
	<label class="custom-checkbox" for="remember">
        <input class="sr-only" id="remember" type="checkbox">
        <span>I have read and <strong class="text-red">agree the terms & conditions</strong></span>
    </label>
</div>
<button class="btn btn--rounded btn--red btn--block">Send Message</button>
