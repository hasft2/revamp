<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div id="site-top" class="parallax sr-only"></div>
<section class="section section-page-banner section--clear">
	<div class="parallax-window" data-parallax="scroll" data-image-src="assets/img/slide-1.jpg"></div>
</section>
<section class="section section-page-content">
	<div class="container">
		<h3 class="h3 section-page__title text-jumbo">Blog</h3>
		<div class="bzg">
			<div class="bzg_c" data-col="m4">
				<aside class="aside aside-nav aside-about">
					<?php include 'include/recent-post.php'; ?>
					<span class="extra-space"></span>
					<?php include 'include/categories.php'; ?>
					<span class="extra-space"></span>
					<a href="" class="ad">
						<img src="http://placehold.it/300x200" alt="" class="img-full">
					</a>
				</aside>
			</div>
			<div class="bzg_c" data-col="m1"></div>
			<div class="bzg_c" data-col="m7">
				<div class="section-page__content">
					<article class="section-page__post-list">
						<?php for ($i=0; $i < 1; $i++) { ?>
							<div class="post">
								<figure>
									<a href="">
										<img src="http://placehold.it/600x300" alt="" class="img-full">
									</a>
								</figure>
								<div class="meta block">
									<h3 class="post__title">
										<a href="">Lorem ipsum dolor sit amet.</a>
									</h3>
									<small>Apr 22 2016</small>
								</div>
								<div class="post__content block">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat soluta laboriosam, possimus repellat esse quidem pariatur deleniti quis dicta ex odit explicabo quaerat nam maxime totam eos temporibus! Ducimus, quod!</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum eos repellat id atque aut porro sequi vel, harum veniam officiis nemo molestiae doloribus minus odio facilis quibusdam illo nihil maiores.</p>
								</div>
								<div class="post__footer block">
									<hr class="hr--style-one block">
									<div class="v-center v-center--spread">
										<span class="text-red">Lifestyle, Media</span>
										<div class="share-social v-center">
											<span class="share-text text-red">Share</span>
											<a href="">
												<i class="fa fa-facebook" aria-hidden="true"></i>
											</a>
											<a href="">
												<i class="fa fa-twitter" aria-hidden="true"></i>
											</a>
										</div>
									</div>
								</div>
								<div class="extra-space"></div>
							</div>
						<?php } ?>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'include/footer.php'; ?>