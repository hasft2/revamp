<?php include 'include/head.php'; ?>
<?php include 'include/header.php'; ?>
<div id="site-top" class="on-screen parallax sr-only"></div>
<section class="section section-home-slider section--clear">
	<div id="slider-home" class="slider slider--dots-abs-bottom" data-slick='{"arrows": false, "dots": true}'>
		<?php for ($i=0; $i < 2; $i++) { ?>
			<div class="slide__item" style="background-image: url(assets/img/slide-1.jpg);">
				<div class="slide__caption slide__caption--animate slide__caption--bottom">
					<div class="block">
						<p class="text-jumbo"><strong>BLAST YOUR ADS GOING VIRAL.</strong></p>
					</div>
					<a href="" class="btn btn--rounded btn--orange"><i class="fa fa-caret-right"></i> Watch Video</a>
				</div>
			</div>
		<?php } ?>
	</div>
	<div class="fake-slider"></div>
	<button class="btn btn--icon btn--circle btn--circle-outline btn--circle-outline--white custom-arrows custom-arrows-prev" data-target-slider="slider-home">
		<i class="fa fa-chevron-left"></i>
	</button>
	<button class="btn btn--icon btn--circle btn--circle-outline btn--circle-outline--white custom-arrows custom-arrows-next" data-target-slider="slider-home">
		<i class="fa fa-chevron-right"></i>
	</button>
</section>
<section class="section section-info">
	<div class="section-info__container container">
		<div class="info-tab tab--style-one">
		    <div class="tab-navs">
		        <a class="tab-nav text-center is-active" href="#">
		        	<i class="fi fi--big flaticon-speakers"></i>
		        	<p class="h3"><strong>Digital Ads Blaster</strong></p>
		        	<p>Sebar iklan Anda ke ribuan sampai jutaan pengguna gadget secara cepat dan viral melalui kekuatan media sosial.</p>
		        </a>
		        <a class="tab-nav text-center">
		        	<i class="fi fi--big flaticon-targetarea"></i>
		        	<p class="h3"><strong>Smart targeted ads</strong></p>
		        	<p>Sebuah layanan yang mengidentifikasi, melacak dan menyebar video Anda ke penonton yang ditargetkan.</p>
		        </a>
		        <a class="tab-nav text-center" href="#tab3">
		        	<i class="fi fi--big flaticon-moneyhand"></i>
		        	<p class="h3"><strong>Watch and get paid</strong></p>
		        	<p>Adshare juga memberikan keuntungan lebih kepada orang-orang yang menonton, berkomentar dan berbagi iklan.</p>
		        </a>
		    </div>
		    <div class="tab-separator text-center">
			    <img src="assets/img/section-arrwos.png" alt="">	    	
		    </div>
		    <div class="tab-contents">
		        <div class="tab-content is-active" id="tab1">
		        	<div class="block">
		        		<div class="bzg">
		        			<div class="bzg_c" data-col="m6"></div>
		        			<div class="bzg_c" data-col="m6">
		        				<p class="h1 text-red header-font"><strong>How We Do</strong></p>
		        				<span class="extra-space"></span>
		        			</div>
		        			<div class="bzg_c" data-col="m6">
		        				<img src="assets/img/work.png" alt="" class="elem-animate" data-anim-js="bounce-up">	
		        			</div>
		        			<div class="bzg_c space-for-mobile elem-animate" data-anim-js="bounce-up" data-col="m6">
			        			<p class="h1"><strong>Advertiser</strong></p>
			        			<p>Your goal is our first priority. Kami fokus pada dunia periklanan. Melalui platform dan ribuan afiliasi kami, kami dapat memberikan jalur terbaik untuk bisnis Anda dalam membantu iklan Anda hingga viral. Hubungi kami sekarang dan kami akan bekerja dengan Anda untuk mencapai tujuan bisnis Anda.</p>
			        			<div class="v-center">
			        				<a href="" class="btn btn--rounded btn--green">Join Now</a>
			        				<a href="" class="btn btn--icon text-blue">watch video <i class="fa fa-play-circle"></i></a>
			        			</div>
		        			</div>
		        		</div>
		        	</div>
		        	<div class="block">
		        		<div class="bzg">
		        			<div class="bzg_c" data-col="m6">
		        				<img src="assets/img/laptop.png" alt="" class="elem-animate" data-anim-js="bounce-up">	
		        			</div>
		        			<div class="bzg_c space-for-mobile elem-animate" data-anim-js="bounce-up" data-col="m6">
			        			<p class="h1"><strong>Affiliate</strong></p>
			        			<p>Adshare cooperate with netizens who are active in social media such as facebook and twitter. Ya, kami mengundang Anda untuk menjadi mitra kerja kami sebagai afiliasi aktif. Kabar baiknya adalah kami memiliki banyak iklan yang menunggu Anda. Watch the ads and get paid.</p>
			        			<div class="v-center">
			        				<a href="" class="btn btn--rounded btn--green">Join Now</a>
			        				<a href="" class="btn btn--icon text-blue">watch video <i class="fa fa-play-circle"></i></a>
			        			</div>
		        			</div>
		        		</div>
		        	</div>
		        </div>
		        <div class="tab-content" id="tab2">
		        	dasdsadasdsad 2
		        </div>
		        <div class="tab-content" id="tab3">
		        	dasdsadasdsad 3
		        </div>
		    </div>
		</div>
	</div>
</section>
<section class="section section-all-info section--clear">
	<div class="section-all-info__item">
		<?php include 'include/section-affiliate.php'; ?>
	</div>
	<div id="section-info-advertiser" class="section-all-info__item">
		<?php include 'include/section-advertiser.php'; ?>
	</div>
</section>
<section class="section section-how-work">
	<div class="section-how-work__container container text-center">
		<p class="h2 section__title"><strong>How We Work</strong></p>
		<div class="circle-list clearfix">
			<div class="circle bg-orange">
				<div class="circle__content">
					<img src="assets/img/watch-icon.png" class="block" alt="">
					<p class="h3 circle__content__title">Watch</p>
					<p class="circle__content__desc">Take a look at our smart
targeted ads.</p>
				</div>
			</div>
			<i class="fa fa-chevron-right fa-2x"></i>
			<div class="circle bg-blue">
				<div class="circle__content">
					<img src="assets/img/share-icon.png" class="block" alt="">
					<p class="h3 circle__content__title">Share</p>
					<p class="circle__content__desc">Fastest and simplest way
to blast the ads.</p>
				</div>
			</div>
			<i class="fa fa-chevron-right fa-2x"></i>
			<div class="circle bg-green">
				<div class="circle__content">
					<img src="assets/img/get-paid-icon.png" class="block" alt="">
					<p class="h3 circle__content__title">Get Paid</p>
					<p class="circle__content__desc">Earn Cash Today</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section section-innovative">
	<div class="hand-and-phone">
		<img src="assets/img/hand-and-phone.jpg" alt="" class="ken-burn">
	</div>
	<div class="container text-center">
		<img src="assets/img/mac.png" alt="" class="mac-img elem-animate block" data-anim-js="bounce-up">
		<p class="h2 section__title"><strong>Innovative services and solutions</strong> start here.</p>
		<p>Adshare telah membangun tim eksekutif terlatih dengan lebih dari 12 tahun pengalaman dalam bidang kreatif, media, digital, bisnis dan teknik perangkat lunak. Berkomitmen dengan visi yang sama, Adshare terus melibatkan advertiser dengan menciptakan solusi cerdas untuk menjawab kebutuhan dunia advertising saat ini.</p>
	</div>
</section>
<hr class="hr--style-one">
<section class="section section-running-ads text-center">
	<p class="h2 section__title"><strong>Running Ads</strong></p>
	<p>These ads are waiting for you. <a href=""><strong>Join now!</strong></a></p>
	<div id="slider-running-ads" class="slider slider-carousel slider-carousel--center thumbs-style-one" data-slick='{"centerMode": true, "arrows": false, "focusOnSelect": true, "centerPadding": "60px", "dots": true}'>
		<?php for ($i=0; $i < 7; $i++) { ?> 
			<div class="thumb">
                <div>
                    <div class="thumb__img" style="background-image: url(assets/img/sample-img-vid-thumb.jpg)">
                    	<button class="btn btn--icon thumb-icon-play">
                            <i class="icon fi flaticon-play h1"></i>
                        </button>
                        <span class="duration">00:30</span>
                    </div>
                    <div class="thumb__info">
                        <a href="" class="thumb__title flex-container">
                            <span>Thumb title maskdmsakd dmaskdmaskd daskdmaskdmsad dsamkdmas</span>
                        </a>
                        <div class="thumb__info__footer">
                            <small class="thumb__info__brand">by Buavita.co.id</small>
                            <small class="thumb__info__total-played text-red">0 played</small>
                        </div>
                    </div>
                </div>
            </div>
		<?php } ?>
	</div>
	<div class="extra-space"></div>
</section>
<hr class="hr--style-one">
<section class="section section-partner">
	<div class="container text-center">
		<p class="h2 section__title"><strong>Our Clients</strong></p>
		<div class="flexblock block">
			<?php
                $partnersLogo = [
                		'logo-bmc.jpg',
                		'logo-friso.jpg',
                		'logo-futanlux.jpg',
                		'logo-kyt.jpg',
                		'logo-maxtron.jpg',
                		'logo-mds.jpg',
                		'logo-morin.jpg',
                		'logo-united.jpg'
                ];
            ?>
			<?php for ($i=0; $i < sizeof($partnersLogo); $i++) { ?>
				<a href="" class="partner-logo">
					<img src="assets/img/<?= $partnersLogo[$i] ?>" alt="">
				</a>
			<?php } ?>
		</div>
	</div>
	<div class="extra-space"></div>
</section>
<section class="section section-contact section--clear">
	<div class="bzg">
		<div class="bzg_c" data-col="m6">
			<img src="assets/img/building.jpg" alt="" class="img-block">		
		</div>
		<div class="bzg_c" data-col="m6">
			<div class="contact-block">
				<p class="h2 section__title"><strong>Contact Us</strong></p>
				<p>If you have any questions, we'd love to talk to you.</p>
				<form action="" class="form--style-one">
					<?php include 'include/form-contact.php'; ?>
				</form>
			</div>
		</div>
	</div>
</section>

<?php include 'include/footer.php'; ?>